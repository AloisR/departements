from django.urls import path

from . import views

app_name = 'departments'
urlpatterns = [
    # ex: /departments/
    path('', views.index, name='index'),
    # ex: /departments/5/
    path('<int:question_id>/', views.detail, name='detail'),
    # ex: /departments/5/results/
    path('<int:question_id>/results/', views.results, name='results'),
    # ex: /departments/5/vote/
    path('<int:question_id>/vote/', views.vote, name='vote'),
]