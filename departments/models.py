import datetime
from django.db import models
from django.utils import timezone

# Trois étapes pour effectuer des modifications aux modèles
# Modifiez les modèles (dans models.py).
# Exécutez python manage.py makemigrations pour créer des migrations correspondant à ces changements.
# Exécutez python manage.py migrate pour appliquer ces modifications à la base de données.

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text