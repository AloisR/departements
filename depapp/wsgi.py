"""
WSGI config for depapp project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

# Point d'entrée pour les serveurs Web compatibles wsgi pour déployer le projet

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'depapp.settings')

application = get_wsgi_application()
