"""
ASGI config for depapp project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/
"""


# Point d'entrée pour les serveurs Web compatibles asgi pour déployer le projet

import os

from django.core.asgi import get_asgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'depapp.settings')

application = get_asgi_application()
